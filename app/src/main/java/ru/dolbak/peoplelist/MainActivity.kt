package ru.dolbak.peoplelist


import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var firstNames: Array<String>
    private lateinit var secondNames: Array<String>
    private lateinit var telegramLogins: Array<String>
    private lateinit var listView: ListView
    private val contacts = ArrayList<Contact>()
    private lateinit var adapter: MyAdapter

    private fun isAppAvailable(context: Context, appName: String?): Boolean {
        val pm: PackageManager = context.getPackageManager()
        return try {
            pm.getPackageInfo(appName!!, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: NameNotFoundException) {
            false
        }
    }

    private fun generateContact(): Contact{
        val random = Random()
        val firstName = firstNames[random.nextInt(firstNames.size)]
        val secondName = secondNames[random.nextInt(secondNames.size)]
        val telegramLogin = telegramLogins[random.nextInt(telegramLogins.size)]
        var mBitmap = BitmapFactory.decodeResource(resources, R.drawable.avatars)
        mBitmap = Bitmap.createBitmap(mBitmap, random.nextInt(6) * mBitmap.width / 6,
            random.nextInt(5) * mBitmap.height / 5, mBitmap.width / 6,
            mBitmap.height / 5)
        return Contact(firstName, secondName, "@$telegramLogin", mBitmap)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listView = findViewById(R.id.listView)
        firstNames = resources.getStringArray(R.array.first_names)
        secondNames = resources.getStringArray(R.array.second_names)
        telegramLogins = resources.getStringArray(R.array.telegram_logins)


        for (i in 0..5){
            contacts.add(generateContact())
        }
        adapter = MyAdapter(contacts)
        listView.adapter = adapter

        listView.setOnItemClickListener{p1, p2, idx, p4 ->
            val contact = contacts[idx]
            val appName = "org.telegram.messenger"
            val isAppInstalled = isAppAvailable(applicationContext, appName)
            intent = if (isAppInstalled) {
                Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=${contact.telegram.substring(1)}"))
            } else {
                Intent(Intent.ACTION_VIEW, Uri.parse("http://www.telegram.me/${contact.telegram.substring(1)}"))
            }
            startActivity(intent)
        }
    }

    fun onClick(view: View) {
        contacts.add(generateContact())
        adapter.notifyDataSetChanged()
    }
}