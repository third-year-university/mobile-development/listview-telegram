package ru.dolbak.peoplelist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class MyAdapter(private val list: ArrayList<Contact>): BaseAdapter() {
    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(idx: Int): Contact {
        return list[idx]
    }

    override fun getItemId(idx: Int): Long {
        return idx.toLong()
    }

    override fun getView(idx: Int, convertView: View?, parent: ViewGroup?): View {
        val viewHolder: ViewHolder
        if (convertView == null){
            viewHolder = ViewHolder()
            val view = LayoutInflater.from(parent?.context).inflate(R.layout.list_view_elem, parent, false)
            viewHolder.parent = view
            viewHolder.firstName = view.findViewById(R.id.firstName)
            viewHolder.secondName = view.findViewById(R.id.secondName)
            viewHolder.email = view.findViewById(R.id.email)
            viewHolder.avatar = view.findViewById(R.id.imageView)
            view.tag = viewHolder
        }
        else{
            viewHolder = convertView.tag as ViewHolder
        }
        viewHolder.firstName.text = getItem(idx).firstName
        viewHolder.secondName.text = getItem(idx).secondName
        viewHolder.email.text = getItem(idx).telegram
        viewHolder.avatar.setImageBitmap(getItem(idx).avatar)


        return viewHolder.parent

    }
    class ViewHolder{
        lateinit var parent: View
        lateinit var firstName: TextView
        lateinit var secondName: TextView
        lateinit var email: TextView
        lateinit var avatar: ImageView
    }
}
