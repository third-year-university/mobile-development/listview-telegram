package ru.dolbak.peoplelist

import android.graphics.Bitmap

data class Contact(val firstName: String, val secondName: String, val telegram: String, val avatar: Bitmap){

}